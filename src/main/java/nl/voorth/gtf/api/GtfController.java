package nl.voorth.gtf.api;

import org.springframework.web.bind.annotation.*;

import java.io.*;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

@RestController
public class GtfController
{
  private static final String GTF_URL = "https://jtv.home.xs4all.nl/gtf/GPL_TLA_FAQ";

  @GetMapping(value = {"/gtf", "/gtf/{tla}"})
  public List<String> findTla(@PathVariable(value = "tla", required = false) Optional<String> tla)
    throws IOException
  {
    var url = new URL(GTF_URL);
    var stream = url.openStream();
    var in = new InputStreamReader(stream);
    var reader = new BufferedReader(in);

    var actualTla = tla.orElse("");
    System.out.println(String.format("actualTla = '%s'", actualTla));
    return reader
      .lines()
      .filter(l -> l.substring(0, actualTla.length()).equalsIgnoreCase(actualTla))
      .map(l -> l.substring(4))
      .collect(Collectors.toList());
  }
}
