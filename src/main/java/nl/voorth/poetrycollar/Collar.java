package nl.voorth.poetrycollar;

import one.util.streamex.StreamEx;
import org.jfree.graphics2d.svg.*;

import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;
import java.awt.geom.*;
import java.util.List;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.*;

import static java.awt.BasicStroke.*;
import static java.awt.Font.BOLD;
import static java.awt.geom.Arc2D.OPEN;
import static java.awt.geom.Arc2D.PIE;
import static java.lang.Math.*;
import static java.util.stream.Collectors.*;


public class Collar
{

  private final SVGGraphics2D canvas = new SVGGraphics2D(297, 210, SVGUnits.MM);
  private final Point2D.Double origin = new Point2D.Double(148.5, 105);

  private final boolean preview;
  private final List<String> text;

  private Font font = new Font("SansSerif", BOLD, 8);
  private float lineHeight = 6f;
  private float strokeWidth = 1.25f;
  private double baseRadius = 80;

  public Collar(List<String> text, boolean preview)
  {
    this.text = text;
    this.preview = preview;
    canvas.setFontSizeUnits(SVGUnits.MM);
  }

  record Test(String a, String b)
  {
  }

  private class Pair<K>
  {
    final K first;
    final K last;

    Pair(K first, K last)
    {
      this.first = first;
      this.last = last;
    }

    public String toString()
    {
      return first.toString() + "," + last.toString();
    }
  }

  private class TextPart
  {
    final double baseRadius;
    final GlyphVector glyphs;
    final double startAngle;
    final LinkedList<String> parts;

    public TextPart(FontRenderContext context, double baseRadius, String text, double startAngle)
    {
      this(context, baseRadius, partsOf(text), startAngle);
    }

    public TextPart(FontRenderContext context, double baseRadius, LinkedList<String> parts, double startAngle)
    {
      this(baseRadius, parts, font.createGlyphVector(context, String.join("", parts)), startAngle);
    }

    public TextPart(double baseRadius, LinkedList<String> parts, GlyphVector glyphs, double startAngle)
    {
      this.baseRadius = baseRadius;
      this.parts = parts;
      this.glyphs = glyphs;
      this.startAngle = startAngle;
    }

    public double width()
    {
      return glyphs.getLogicalBounds().getWidth();
    }
  }


  private static Area addArea(Area acc, Area area)
  {
    acc.add(area);
    return acc;
  }

  public Collar withRadius(double radius)
  {
    baseRadius = radius;
    return this;
  }

  public Collar withStrokeWidth(float strokeWidthMm)
  {
    strokeWidth = strokeWidthMm;
    return this;
  }

  public Collar withFont(String name, int size, EnumSet<FontStyle> fontStyle)
  {
    font = new Font(
      name,
      fontStyle.stream()
        .mapToInt(FontStyle::ordinal)
        .sum(),
      size);
    canvas.setFont(font);
    lineHeight = (float) canvas.getFont()
      .createGlyphVector(canvas.getFontRenderContext(), "X")
      .getVisualBounds()
      .getHeight();
    return this;
  }


  static Arc2D.Double arc(Point2D.Double origin, double radius, double angle, double extent)
  {
    return arc(origin, radius, angle, extent, OPEN);
  }

  static Arc2D.Double arc(Point2D.Double origin, double radius, double angle, double extent, int arcType)
  {
    return new Arc2D.Double(origin.x - radius, origin.y - radius, 2 * radius, 2 * radius, toDegrees(angle), toDegrees(extent), arcType);
  }

  static Arc2D.Double arc(Point2D.Double origin, double radius, double angle)
  {
    return arc(origin, radius, angle, toRadians(540) - 2 * angle);
  }

  public String asSvg()
  {
    canvas.setFont(font);
    var context = canvas.getFontRenderContext();
    var laserCut = new BasicStroke(preview ? 0.1f : 0.01f, CAP_SQUARE, JOIN_ROUND);

    Area totalArea = getArea(context);

    if (preview)
    {
      canvas.setPaint(Color.CYAN.darker());
      canvas.fill(totalArea);
    }
    canvas.setStroke(laserCut);
    canvas.setPaint(preview ? Color.BLACK : Color.RED);
    canvas.draw(totalArea);

    drawInfo();

    var viewBox = new ViewBox(0, 0, canvas.getWidth(), canvas.getHeight());
    var svgElement = canvas.getSVGElement("test", true, viewBox, null, null);
    canvas.dispose();
    return svgElement;
  }

  private void drawInfo()
  {

    // remember canvas settings
    var oldFont = canvas.getFont();
    var oldPaint = canvas.getPaint();

    var fontInfo = String.format("font: %s %d", font.getName(), font.getSize());
    var radiusInfo = String.format("baseRadius: %.0fmm", baseRadius);
    var infoFont = new Font("Monospaced", Font.PLAIN, 1);
    canvas.setFont(infoFont);
    canvas.setPaint(Color.BLACK);

    // draw crosshair at the origin
    canvas.draw(new Line2D.Double(origin.x - 2, origin.y, origin.x + 2, origin.y));
    canvas.draw(new Line2D.Double(origin.x, origin.y - 2, origin.x, origin.y + 2));

    var height = canvas.getFontMetrics(infoFont)
      .getHeight();
    var x = (float) origin.x;
    AtomicReference<Float> y = new AtomicReference<>((float) origin.y);
    Stream.of(fontInfo, radiusInfo)
      .forEach(line -> canvas.drawString(line, x, y.updateAndGet(v -> (v + height * 2f))));

    // restore canvas settings
    canvas.setPaint(oldPaint);
    canvas.setFont(oldFont);
  }

  private Area getArea(FontRenderContext context)
  {
    var topRadius = baseRadius - lineHeight;
    var bottomRadius = baseRadius + lineHeight;
    var baseAngle = 140;

    var lines = text.stream()
      .map(line -> line.replaceAll("-", " "))
      .map(String::toUpperCase)
      .collect(toCollection(LinkedList::new));

    // determine line overlap
    var pairs = StreamEx.of(lines)
      .pairMap(Pair::new)
      .collect(toList());


    var radius = baseRadius;
    var nextRadius = baseRadius + lineHeight;

    var textPart = new TextPart(context, baseRadius, partsOf(lines.getFirst()), 0d);
    var textParts = new LinkedList<TextPart>(List.of(textPart));
    var totalAngle = textPart.glyphs.getLogicalBounds().getWidth() / radius;

    for (var pair : pairs)
    {
      var topWidth = textPart.glyphs.getLogicalBounds().getWidth();
      var topRight = font.createGlyphVector(context, textPart.parts.getLast());
      var topRightWidth = topRight.getLogicalBounds().getWidth();

      var bottomText = partsOf(pair.last);

      var bottomGlyphs = font.createGlyphVector(context, String.join("", bottomText));
      var bottomWidth = bottomGlyphs.getLogicalBounds().getWidth();

      var overlapAngle = topRightWidth / radius;

      var startAngle = totalAngle - overlapAngle;
      totalAngle += bottomWidth / radius - overlapAngle;

      textPart = new TextPart(radius, bottomText, bottomGlyphs, startAngle);
      textParts.add(textPart);
      radius += lineHeight;

    }


    var marginAngle = toRadians(baseAngle);
    var marginEndAngle = toRadians(540) - marginAngle;
    var marginExtentAngle = marginEndAngle - marginAngle;

    var topWidth = textParts.getFirst().width();
    var topStartAngle = toRadians(270) - totalAngle / 2;
    var topExtentAngle = topStartAngle - marginAngle + topWidth / baseRadius;

    var bottomEndAngle = toRadians(270) + totalAngle / 2;
    var bottomStartAngle = bottomEndAngle - textParts.getLast().width() / bottomRadius;
    var bottomExtentAngle = marginAngle + marginExtentAngle - bottomStartAngle;

    var topAdjust = 0d;
    var baseAdjust = adjustForStroke(topRadius);
    var bottomAdjust = adjustForStroke(baseRadius);

    var topArc = arc(origin, topRadius, marginAngle, topExtentAngle + topAdjust);
    var baseArc = arc(origin, baseRadius, marginAngle + baseAdjust, marginExtentAngle - baseAdjust);
    var bottomArc = arc(origin, bottomRadius, bottomStartAngle, bottomExtentAngle - bottomAdjust);

    canvas.setStroke(new BasicStroke(0.2f, CAP_ROUND, JOIN_ROUND));

    var outlineStroke = new BasicStroke(strokeWidth, CAP_SQUARE, JOIN_ROUND);
    var topArea = new Area(outlineStroke.createStrokedShape(topArc));
    var baseArea = new Area(outlineStroke.createStrokedShape(baseArc));
    var bottomArea = new Area(outlineStroke.createStrokedShape(bottomArc));

    var startPointArea = endPointArea(origin, topRadius, marginAngle - (strokeWidth / topRadius) / 2);
    var endPointArea = endPointArea(origin, baseRadius, marginEndAngle + (strokeWidth / baseRadius) / 2);

    var startGlyphs = font.createGlyphVector(context, "((((((((((");
    var endGlyphs = font.createGlyphVector(context, "))))))))))");
    var textArea = addAreas(
      getArea(textParts.getFirst().glyphs, origin, baseRadius, totalAngle / 2),
      getArea(textParts.getLast().glyphs, origin, baseRadius + lineHeight, -totalAngle / 2),
      getArea(startGlyphs, origin, baseRadius, toRadians(260 - baseAngle)),
      getArea(endGlyphs, origin, baseRadius + lineHeight, toRadians(baseAngle - 260))
                           );

    var clipArea = addAreas(
      clipArea(origin, topRadius, marginAngle, topExtentAngle),
      clipArea(origin, baseRadius, bottomStartAngle, bottomExtentAngle - bottomAdjust)
                           );
    clipArea.intersect(textArea);

    return addAreas
      (
        clipArea,
        topArea,
        baseArea,
        bottomArea,
        startPointArea,
        endPointArea
      );
  }

  private static LinkedList<String> partsOf(String last)
  {
    return Stream.of(last.split("/"))
      .collect(toCollection(LinkedList::new));
  }

  private Area endPointArea(Point2D.Double origin, double topRadius, double angle)
  {
    var unitCircle = new Ellipse2D.Double(-1, -1, 2, 2);
    var transform = new AffineTransform();
    transform.translate(origin.x + (topRadius + (lineHeight + strokeWidth) / 2) * cos(angle), origin.y - (topRadius + (lineHeight + strokeWidth) / 2) * sin(angle));
    transform.scale(lineHeight / 2, lineHeight / 2);
    var endPoint = transform.createTransformedShape(unitCircle);

    var stroke = new BasicStroke(strokeWidth * 2, CAP_SQUARE, JOIN_ROUND);
    return new Area(stroke.createStrokedShape(endPoint));
  }

  private Area clipArea(Point2D.Double origin, double radius, double startAngle, double extentAngle)
  {
    var topArc = arc(origin, radius, startAngle, extentAngle, PIE);
    var bottomArc = arc(origin, radius + lineHeight, startAngle, extentAngle, PIE);
    var area = new Area(bottomArc);
    area.subtract(new Area(topArc));
    return area;
  }

  private Area addAreas(Area start, Area... areas)
  {
    return Stream.of(areas)
      .reduce(start, Collar::addArea);
  }

  private double adjustForStroke(double radius)
  {
    var theta = acos(lineHeight / (lineHeight + 2 * strokeWidth));
    var adjustment = sin(theta) * (lineHeight / 2 + strokeWidth);
    return adjustment / radius;
  }

  private static Area emptyArea()
  {
    return new Area(new Line2D.Double(0, 0, 0, 0));
  }

  private Area getArea(GlyphVector glyphs, Point2D.Double origin, double radius, double angle)
  {

    var numGlyphs = glyphs.getNumGlyphs();
    var width = glyphs
      .getLogicalBounds()
      .getWidth();

    var start = (signum(angle) * width - width) / 2;

    Area area = new Area(new Line2D.Double(origin, origin));
    var advance = 0d;
    for (int i : glyphs.getGlyphCharIndices(0, numGlyphs, null))
    {
      var metrics = glyphs.getGlyphMetrics(i);
      var shift = advance + metrics.getBounds2D()
        .getCenterX();
      var glyphShape = glyphs.getGlyphOutline(i, (float) (origin.x - shift), (float) (origin.y + radius));
      var theta = angle - (shift + start) / radius;
      var rot = AffineTransform.getRotateInstance(theta, origin.x, origin.y);

      var glyphArea = new Area(glyphShape);
      area.add(glyphArea.createTransformedArea(rot));
      advance += metrics.getAdvance();
    }
    return area;
  }

}
