package nl.voorth.poetrycollar;

import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.io.IOException;
import java.util.*;
import java.util.List;

@RestController
public class CollarController
{

  public static final String DEFAULT_TEXT = "THE QUICK /BROWN FOX,JUMPS OVER THE LAZY DOG";

  @GetMapping("/fonts")
  public List<String> fonts()
  {
    var availableFontFamilyNames =
      GraphicsEnvironment
        .getLocalGraphicsEnvironment()
        .getAvailableFontFamilyNames();
    return Arrays.asList(availableFontFamilyNames);
  }

  @GetMapping("/collar")
  public String collar(
      @RequestParam(name = "preview", defaultValue="true" ) boolean preview,
      @RequestParam(name = "text", defaultValue= DEFAULT_TEXT) List<String> text,
      @RequestParam(name = "radius", defaultValue = "80") double radius,
      @RequestParam(name = "stroke", defaultValue = "1.25") float strokeWidth,
      @RequestParam(name = "fontname", defaultValue = "SansSerif") String fontName,
      @RequestParam(name = "fontsize", defaultValue = "9") int fontSize,
      @RequestParam(name = "fontstyle", defaultValue = "BOLD") EnumSet<FontStyle> fontStyle
    )
  {
    return new Collar(text, preview)
      .withFont(fontName, fontSize, fontStyle)
      .withRadius(radius)
      .withStrokeWidth(strokeWidth)
      .asSvg();
  }

  @GetMapping("/collar/{filename}")
  public void download(
    @RequestParam(name="preview", defaultValue="false" ) boolean preview,
    @RequestParam(name="text", defaultValue= DEFAULT_TEXT) List<String> text,
    @PathVariable("filename") String filename,
    HttpServletResponse response
  ) throws IOException
  {
    var content = new Collar(text, preview).asSvg().getBytes();

    response.setContentType("application/octet-stream");
    response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s.svg\"", filename));
    response.setContentLength(content.length);
    try(var output = response.getOutputStream())
    {
      output.write(content);
    }
  }

}

