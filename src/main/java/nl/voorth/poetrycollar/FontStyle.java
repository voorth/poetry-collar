package nl.voorth.poetrycollar;

public enum FontStyle
{
  PLAIN, BOLD, ITALIC
}
