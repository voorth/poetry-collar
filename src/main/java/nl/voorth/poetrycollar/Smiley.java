package nl.voorth.poetrycollar;

import org.jfree.graphics2d.svg.SVGGraphics2D;
import org.jfree.graphics2d.svg.SVGUnits;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.awt.*;
import java.awt.geom.Arc2D;
import java.awt.geom.Ellipse2D;

@RestController
public class Smiley
{
  @GetMapping("/smile")
  public String smile()
  {
    var g = new SVGGraphics2D(300, 300, SVGUnits.PX);

    var face = new Ellipse2D.Double(10, 10, 200, 200);
    g.setPaint(Color.yellow);
    g.fill(face);
    g.setPaint(Color.black);
    g.draw(face);

    var eye1 = new Ellipse2D.Double(50,65,20,40);
    var eye2 = new Ellipse2D.Double(150,65,20,40);

    g.setPaint(Color.BLACK);
    g.fill(eye1);
    g.fill(eye2);

    //create a wider line
    Stroke stroke = new BasicStroke(5);
    g.setStroke(stroke);
    Arc2D arc = new Arc2D.Double(55, 70, 110, 110, 190, 160, Arc2D.OPEN);
    g.draw(arc);


    return g.getSVGElement();


  }
}
